var express = require('express');
var app = express();
var mysql = require('mysql');
var path    = require("path");
var cons = require('consolidate');
var bodyParser = require('body-parser')

app.engine('html', cons.underscore);
app.set('view engine', 'html');
app.set('views',__dirname + '/views');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


var connection = mysql.createConnection({
  host: 'localhost',
  user: 'blue',
  password: 'blue',
  database: 'deva'
})

var db_res;

function run_db() 
{
        connection.query('SELECT * FROM contacts', function(err, results) {
            if (err) throw err
           db_res=results;
        })

}

run_db();

app.get('/list',function(req,res) 
        {
            run_db();
            res.render('index.html', { addresses: db_res }, function(err, html) 
                {
                    if(err) 
                    {
                        console.log(err);
                    }
                    res.send(html);

                });   
             
        });

app.get('/new',function(req,res)
        {
            res.render('new.html');
        });

app.post('/add', function(req, res)
        {
                var dat=req.body;
            
                var keys = Object.keys(dat);
                var arr = [];
                var temp = [];
                for(var i =0 ;i < keys.length; i++){
                    temp.push(dat[keys[i]]);
                    if(i % 4 == 3){
                        arr.push(temp);
                        temp = [];
                    }
                }
            
               connection.query('INSERT INTO contacts (name, address, email, phone) VALUES ?', [arr], function(err) {
                   if(err) console.log(err);
                   res.send("success");
                });
                
               run_db();

        });  

  app.listen(3000, function () {
        console.log(' app listening on port 3000!');
    });


